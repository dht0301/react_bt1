import React, { Component } from "react";
import { dataList } from "./data";
import Item from "./Item";

export default class BaiTap1 extends Component {
  state = {
    shoeList: dataList,
  };
  renderListShoe = () => {
    return this.state.shoeList.map((item, index) => {
      return <Item list={item} key={index} />;
    });
  };
  render() {
    return (
      <div className="container py-5">
        <div className="row">{this.renderListShoe()}</div>;
      </div>
    );
  }
}
