import React, { Component } from "react";

export default class Item extends Component {
  render() {
    console.log(this.props);
    let { name, price, image } = this.props.list;
    return (
      <div className="col-3 p-1">
        <div className="card text-left h-100">
          <img className="card-img-top " src={image} alt="" />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <p className="card-text">{price}</p>
            <button className="btn btn-primary">Find out more</button>
          </div>
        </div>
      </div>
    );
  }
}
